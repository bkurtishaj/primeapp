# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cslist', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='platform',
            name='name',
            field=models.CharField(default=b'OR', max_length=2, choices=[(b'PH', b'Phones'), (b'LA', b'Laptops'), (b'PC', b'PC'), (b'TV', b'TV'), (b'CO', b'Consoles'), (b'OR', b'Other')]),
        ),
    ]
